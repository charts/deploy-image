#!/usr/bin/env bash

set -e

GCR_REGISTRY=${GCR_REGISTRY:-"gcr.io/top-chain-204115/gitlab"}
if [ -z "$TAG" ]; then
  echo "TAG env var required"
  exit 1
fi

for image in $(build-scripts/list-helm-images.sh); do
    SOURCE_IMAGE_NAME="$(echo "${image##*/}" | cut -d':' -f1 | cut -d'@' -f1)"
    MIRRORED_IMAGE_TAG="${GCR_REGISTRY}/${SOURCE_IMAGE_NAME}:$TAG"

    # Override gitlab-shell to be the primary Launcher image
    # https://gitlab.com/charts/deploy-image/issues/25
    if [ "$SOURCE_IMAGE_NAME" == "gitlab-shell" ]; then
      MIRRORED_IMAGE_TAG="gcr.io/top-chain-204115/gitlab:$TAG"
    fi

    docker pull "$image"
    docker tag "$image" "$MIRRORED_IMAGE_TAG"
    docker push "$MIRRORED_IMAGE_TAG"
done
