#!/usr/bin/env bash

set -e

deployer_image="deployer_gitlab_chart"
if [ -n "$CI_REGISTRY_IMAGE" ]; then
    deployer_image="$CI_REGISTRY_IMAGE/$deployer_image"
fi

docker_opts="-e DISABLE_CLOUDLAUNCHER_IMAGE_OVERRIDES=true"
while true; do
  case "$1" in
    --cloudlauncher )
      docker_opts="-e DISABLE_CLOUDLAUNCHER_IMAGE_OVERRIDES="; shift;;
    --help )
      echo "USAGE: list-helm-images.sh";
      echo "List all container images referenced in helm charts";
      echo "ARGUMENTS:";
      echo "  --cloudlauncher       optionally apply cloudlauncher value overrides";
      exit 0;;
    -- ) shift; break ;;
    * ) break ;;
  esac
done

docker run $docker_opts --entrypoint /bin/list_helm_images.sh $deployer_image
