function check_helm_image_references(){
  if build-scripts/list-helm-images.sh --cloudlauncher | grep -v "^$GCR_REGISTRY_PUBLIC"; then
    echo "Found an image reference in the Helm charts outside the blessed Google registry"
    exit 1
  fi
}

function major_version_number() {
  echo "$1" | cut -d"." -f 1
}
