#!/usr/bin/env bash

set -e

function camelCase() {
  IFS='-_' read -r -a parsed_img_name <<< "$1"
  img_name_cap=""
  for i in "${!parsed_img_name[@]}"
  do
      if [ $i -eq 0 ]; then
        img_name_cap="${img_name_cap}${parsed_img_name[$i]}"  
      else
        img_name_cap="${img_name_cap}${parsed_img_name[$i]^}"
      fi
  done
  echo $img_name_cap
}

GCR_REGISTRY=${GCR_REGISTRY:-"gcr.io/top-chain-204115/gitlab"}
if [ -z "$TAG" ]; then
  echo "TAG env var required"
  exit 1
fi

for image in $(build-scripts/list-helm-images.sh); do
    source_image_name="$(echo "${image##*/}" | cut -d':' -f1 | cut -d'@' -f1)"
    output_image_name="$(camelCase $source_image_name)Image"
    mirrored_image_tag="${GCR_REGISTRY}/${source_image_name}:$TAG"

    # Override gitlab-shell to be the primary Launcher image
    # https://gitlab.com/charts/deploy-image/issues/25
    if [ "$source_image_name" == "gitlab-shell" ]; then
      mirrored_image_tag="gcr.io/top-chain-204115/gitlab:$TAG"
      output_image_name="image"
    fi

    cat <<PARAMETER
${output_image_name}:
    type: string
    default: $mirrored_image_tag
    x-google-marketplace:
      type: IMAGE
PARAMETER

done
