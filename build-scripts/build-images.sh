#!/bin/bash

set -e

if [ -z "$TAG" ]; then
  echo "TAG env var required"
  exit 1
fi

cd deployer_helm_local_tiller
docker build -t deployer_helm_local_tiller_base .
cd ../deployer_gitlab_chart
docker build -t deployer_gitlab_chart -f deployer/Dockerfile --build-arg TAG=$TAG .
cd ..
