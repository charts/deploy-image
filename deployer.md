# Gitlab Google Marketplace Deployer

## Overview

This project creates a Google Marketplace compatible Docker deploy image vendoring Gitlab Helm charts. The deploy container runs a local  Tiller process inside the container at deploy time. There is no Tiller installed inside the target Kubernetes cluster and no state maintained across upgrades.

## What does this repo contain?

* Marketplace deploy image resources:
  * [deployer_helm_local_tiller](deploy_helm_local_tiller)
    * Generic local tiller installer base image.
    * Follows closely the pattern set by Google Marketplace example Helm deployer image.
  * [deployer_gitlab_chart](deployer_gitlab_chart)
    * Marketplace deployer image containing a vendored Gitlab charts repo.
    * This is the Marketplace deployer image that gets deployed to Google.
    * Vendored Gitlab charts repo is a Git submodule. The submodule SHA determines which version is vendored.
* Gitlab CI jobs:
  * A build job that runs for any commit except a tag to build and verify the images.
  * A tag job that runs for tags to:
    * Build and verify deploy images.
    * Push deploy image to Google container registry.
    * Mirrors all referenced Docker images to Google containter registry.
* Marketplace deploy simulation scripts
  * [marketplace/deploy.sh](marketplace/deploy.sh)
  * Simulates creating the Kubernetes resources we expect Google Marketplace to handle.
  * Launches the deploy image as either a pod or job and executes a deployment in a target k8s cluster.
  * Follows closely the pattern set by Google Marketplace example app start.sh script and make files without make.

## Development

1. Clone this repo
2. Run `git submodule init && git submodule update`
3. Build `deployer_helm_local_tiller_base` image (see below)
4. Build `deployer_gitlab_chart` image (see below)
5. Run `marketplace/deploy.sh` to simulate a Marketplace deploy

The steps above assume shared context between `kubectl` and `docker`. One easy way to do this is run Minikube and set your Docker context to Minikube's Docker.


## Deploy Image

### deployer_helm_local_tiller_base

This is a base deployer image with everything needed to deploy Helm charts via a local Tiller process. It's based on Alpine to match Gitlab's images.

Note: This image includes a generic `/bin/deploy_helm_local_tiller.sh`. This script currently runs a Helm dry-run for testing purposes. Eventually this will be changed to perform a Helm install.

#### Building the deployer_helm_local_tiller_base image

```
$ cd deployer_helm_local_tiller
$ docker build -t deployer_helm_local_tiller_base .
```

### deployer_gitlab_chart

This directory contains a Dockerfile that vendors a full copy of the Charts repo with Helm dependencies installed. Ultimately this likely belongs in the Charts repo alongside the code. For now it is using a git submodule to clone the Chart repo.

#### Building the deployer_gitlab_chart image

The tag value here is the major version tag number that will applied to Marketplace Google container registry images.

```
$ cd deployer_gitlab_chart
$ docker build -t deployer_gitlab_chart -f deployer/Dockerfile --build-arg TAG=11 .
```

## Marketplace Image Parameters

Image repository and tag strings are populated at deploy time from Google Cloudlauncher parameters. The [schema.yml](deployer_gitlab_chart/schema.yml) file definining the parameters is included in the deployer_gitlab_chart image. These parameters are populated at deploy time in [deploy_helm_local_tiller.sh](deploy_helm_local_tiller/scripts/deploy_helm_local_tiller.sh#L30-42) in the deployer_helm_local_tiller image. The script replaces placeholder variables in the image overrides file and adds the resulting values file to final Helm update command. Previously a static file was passed to the Helm update command. This file [deployer_gitlab_chart/cloudlauncher-image-overrides.yml](deployer_gitlab_chart/cloudlauncher-image-overrides.yml) is still included for reference but is no longer used in the deploy process.

## Simulating the Marketplace Deployer

There is a [helper script](marketplace/deploy.sh) in the repo that simulates a Marketplace deploy. This script uses `kubectl` to  deploy resources to mimic those installed by Marketplace during no-touch deploys. These steps are a best guess from Google's sample.

```
$ ./marketplace/deploy.sh \
    --parameters='{"APP_INSTANCE_NAME":"gitlabdemo","NAMESPACE":"gitlab","domain":"example.local","certmanager_email":"me@example.local","helm_set_overrides":"global.host.domain=somewebsite.biz"}' \
    --deployer=deployer_gitlab_chart \
    --pod=true \
    --disable-cl-overrides
```

## Debugging

#### Disabling Cloud Launcher Value Overrides

A [values file](deployer_gitlab_chart/cloudlauncher.yml) is included in the `deployer_gitlab_chart` to override source images for Google Marketplace deploys. This values file can be removed for local testing and development by passing `--disable-cl-overrides` to the `deploy.sh` script. With this argument the values file will not be included and default public images will be referenced in the Gitlab charts.

The Cloud Launcher overrides file currently sets all image repositories to the Marketplace Google container registry. This will not work locally.

#### Pod vs. Job

The deploy-image runs as a job via Cloud Launcher. The deploy script can optionally launch the deploy container as pod instead of a job for debugging purposes.

Pass `--pod=true` to the deploy.sh script to do this. The pod overrides entrypoint to start up the pod without automatically running the deploy script. Exec into the pod and run `/bin/deploy_helm_local_tiller.sh` to debug.

### Setting Helm values

There are two mechanisms for populating Helm values at deploy time:
1) Well known chart values are populated from Marketplace parameters in a templatized values.yml file. This file is passed to Helm via `-f values.yml`.
2) Arbitrary overrides can also be passed in through a Marketplace string parameter in the format `field.subfield=1,otherfield=True`. These overrides are passed to Helm via `--set`.

A [schema file](deployer_gitlab_chart/schema.yaml) defines the expected Marketplace parameters. A Helm [value file](deployer_gitlab_chart/values.yml.template) is templated within the `deployer_gitlab_chart` image. The template is populated at deploy time from incoming parameter values. We can use this template to map well known parameters to chart values.

## Image Mirroring

All images used by the Gitlab charts deploy must be mirrored to Google container registry. We push images to an intermediary Google project. Google will eventually mirror from there to our public Marketplace registry.

Images are mirrored in a Gitlab CI job here whenever a tag is applied to the repo. A `helm template` command is run via [script](deployer_helm_local_tiller/scripts/list_helm_images.sh) within a deploy-image container instance. All image references are parsed out of the resulting Helm templates. Each image is pulled, image repository url flattened to a single string image name, the tag from this repo is applied, and then pushed up to intermediary Google project container registry.

If images are missing from the intermediary Google project container registry it's likely that the image is not showing up in the default `helm template` installation. Additional Helm values may need to be added to the deploy-image to enable additional chart image references.

### Generate List of Helm Chart Images

The script mentioned above to generate all referenced images can be run locally via [build-scripts/list-helm-images.sh](build-scripts/list-helm-images.sh). The script runs in the context of the deploy-image container so you will need to make sure you have the deploy-image built locally on your Docker host `TAG=11 build-scripts/build-images.sh`.

Output images with Cloud Launcher overrides applied:

```
$ ./build-scripts/list-helm-images.sh --cloudlauncher

launcher.gcr.io/gitlab/gitlab/busybox:11
launcher.gcr.io/gitlab/gitlab/gitlab-gitlab-runner:11
launcher.gcr.io/gitlab/gitlab/gitlab-rails-ee:11
launcher.gcr.io/gitlab/gitlab/gitlab-shell:11
launcher.gcr.io/gitlab/gitlab/gitlab-sidekiq-ee:11
launcher.gcr.io/gitlab/gitlab/gitlab-unicorn-ee:11
...
```

Output original images without Cloudlauncher overrides applied:

```
$ ./build-scripts/list-helm-images.sh

busybox:latest
gitlab/gitlab-runner:alpine-v10.3.0
jimmidyson/configmap-reload:v0.1
k8s.gcr.io/defaultbackend:1.3
minio/mc:latest
...
```
