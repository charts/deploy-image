#!/bin/bash

set -eox pipefail

# This is the entry point for the production deployment

# If any command returns with non-zero exit code, set -e will cause the script
# to exit. Prior to exit, set App assembly status to "Failed".
handle_failure() {
  code=$?
  patch_assembly_phase.sh --status="Failed"
  cat /tmp/tiller.log
  exit $code
}
trap "handle_failure" EXIT

/bin/expand_config.py

APP_INSTANCE_NAME="$(/bin/print_config.py --param '{"x-google-marketplace": {"type": "NAME"}}')"
NAMESPACE="$(/bin/print_config.py --param '{"x-google-marketplace": {"type": "NAMESPACE"}}')"
HELM_SET_OVERRIDES="$(/bin/print_config.py --param '{"name": "helm_set_overrides"}')"
RELEASE="$APP_INSTANCE_NAME"

# Run tiller in the background
./bin/tiller >/tmp/tiller.log 2>&1  &

# Always override certain chart defaults that we know don't work
helm_values_option="-f /data/cloudlauncher/cloudlauncher-default-overrides.yml"

# Image overrides from parameters
images_template="/data/cloudlauncher/cloudlauncher-image-overrides.yml.template"
images_values_file="/data/cloudlauncher/cloudlauncher-image-overrides.yml"
if [ -f "$images_template" ]; then
    # Parse Google image parameters into separate *_repo, *_tag variables
    eval $(/bin/load_image_vars.sh)
    env
    # Inject variable values into template
    cat "$images_template" \
      | envsubst \
      > $images_values_file
    helm_values_option="$helm_values_option -f $images_values_file"
fi

# Generate Helm values file from user inputs
helm_values_template="/data/helm/values.yml.template"
helm_values="/data/helm/values.yml"
if [ -f "$helm_values_template" ]; then
    env_vars="$(/bin/print_config.py -o shell_vars)"
    cat "$helm_values_template" \
      | /bin/config_env.py envsubst "${env_vars}" \
      > $helm_values
    helm_values_option="$helm_values_option -f $helm_values"
fi

# Generate --set option if any values were passed in
helm_set_option=""
if [ -n "$HELM_SET_OVERRIDES" ]; then
    helm_set_option="--set $HELM_SET_OVERRIDES"
fi

# Update the Application with our componentKinds
# --output=json is used to force kubectl to succeed even if the patch command
# makes not change to the resource. Otherwise, this command exits 1.
kubectl patch "applications/$APP_INSTANCE_NAME" \
  --output=json \
  --namespace="$NAMESPACE" \
  --type=merge \
  --patch "{\"spec\": {\"componentKinds\": [{\"apiVersion\":\"v1\",\"kind\":\"ConfigMap\"},{\"apiVersion\":\"v1\",\"kind\":\"Service\"},{\"apiVersion\":\"v1\",\"kind\":\"ServiceAccount\"},{\"apiVersion\":\"rbac.authorization.k8s.io/v1\",\"kind\":\"Role\"},{\"apiVersion\":\"rbac.authorization.k8s.io/v1\",\"kind\":\"ClusterRole\"},{\"apiVersion\":\"rbac.authorization.k8s.io/v1\",\"kind\":\"RoleBinding\"},{\"apiVersion\":\"rbac.authorization.k8s.io/v1\",\"kind\":\"ClusterRoleBinding\"},{\"apiVersion\":\"batch/v1\",\"kind\":\"Job\"},{\"apiVersion\":\"v1\",\"kind\":\"Secret\"},{\"apiVersion\":\"extensions/v1beta1\",\"kind\":\"Ingress\"},{\"apiVersion\":\"v1\",\"kind\":\"PersistentVolumeClaim\"},{\"apiVersion\":\"apps/v1beta2\",\"kind\":\"Deployment\"},{\"apiVersion\":\"autoscaling/v2beta1\",\"kind\":\"HorizontalPodAutoscaler\"},{\"apiVersion\":\"apps/v1beta2\",\"kind\":\"StatefulSet\"},{\"apiVersion\":\"policy/v1beta1\",\"kind\":\"PodDisruptionBudget\"}]}}"

/bin/helm upgrade --namespace $NAMESPACE --install $RELEASE --wait --timeout 600 /data/chart/ $helm_values_option $helm_set_option

patch_assembly_phase.sh --status="Success"

trap - EXIT
