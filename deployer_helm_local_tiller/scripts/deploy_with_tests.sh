#!/bin/bash

set -eox pipefail

# This is the entry point for the deploying with tests included

# First deploy as normal
. /bin/deploy_helm_local_tiller.sh

# Then run helm tests
/bin/helm test $RELEASE
