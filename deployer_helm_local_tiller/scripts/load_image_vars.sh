#!/bin/bash

output_var_names=""
output_assignements="TRUE"
var_name_list=""

for i in "$@"
do
case $i in
  --varnames)
    output_var_names="TRUE"
    output_assignements=""
    shift
    ;;
  *)
    >&2 echo "Unrecognized flag: $i"
    exit 1
    ;;
esac
done

# Parse image parameters into separate *_repo and *_tag variables
for env_var in $(/bin/print_config.py -o shell_vars); do
    if echo "$env_var" | grep -i "Image" > /dev/null; then
        image_var="${env_var#"$"}"
        image_param_value="$(/bin/print_config.py --param "{\"name\": \"${env_var#"$"}\"}")"
        image_param_repo="$(echo $image_param_value | cut -d':' -f 1)"
        image_param_tag="$(echo $image_param_value | cut -d':' -f 2)"

        if [ -n "$output_assignements" ]; then
            echo "export ${env_var#"$"}_repo=\"$image_param_repo\""
            echo "export ${env_var#"$"}_tag=\"$image_param_tag\""
        fi

        # Build list of variable names
        test -n "$var_name_list" && var_name_list="$var_name_list "
        var_name_list="${var_name_list}\$${env_var#"$"}_repo \$${env_var#"$"}_tag"
    fi
done

test -n "$output_var_names" && echo "$var_name_list"
