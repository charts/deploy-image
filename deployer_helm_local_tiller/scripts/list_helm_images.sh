#!/bin/bash

set -exo pipefail

# Always override certain chart defaults that we know don't work
helm_values_option="-f /data/cloudlauncher/cloudlauncher-default-overrides.yml"

# Override images to use images hosted by launcher.gcr.io
if [ -f /data/cloudlauncher/cloudlauncher-image-overrides.yml -a -z "$DISABLE_CLOUDLAUNCHER_IMAGE_OVERRIDES" ]; then
   helm_values_option="$helm_values_option -f /data/cloudlauncher/cloudlauncher-image-overrides.yml"
fi

/bin/helm template /data/chart/ $helm_values_option --set certmanager-issuer.email=none@none.com | \
   yq -r ". | select( .kind == \"Job\" or .kind == \"Deployment\" or .kind == \"StatefulSet\" or .kind == \"DaemonSet\" ) | .spec.template.spec | [.containers,.initContainers] | .[] | select(.!=null) | .[].image" | \
   sort | uniq
