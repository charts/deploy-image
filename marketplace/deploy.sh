#!/bin/bash
#
# Copyright 2018 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# This script simulates a Marketplace deploy

set -eo pipefail

debug_pod=""

# --disable-cl-overrides maps to an environment variable used to disable Cloud Launcher values file
# overrides in dev mode. This is not intended to be used in live Cloud Launcher deploys.
disable_cl_overrides=""

for i in "$@"
do
case $i in
  --deployer=*)
    deployer="${i#*=}"
    shift
    ;;
  --parameters=*)
    parameters="${i#*=}"
    shift
    ;;
  --entrypoint=*)
    entrypoint="${i#*=}"
    shift
    ;;
  --pod=true)
    debug_pod="TRUE"
    shift
    ;;
  --disable-cl-overrides)
    disable_cl_overrides="TRUE"
    shift
    ;;
  *)
    >&2 echo "Unrecognized flag: $i"
    exit 1
    ;;
esac
done

[[ -z "$deployer" ]] && >&2 echo "--deployer required" && exit 1
[[ -z "$parameters" ]] && >&2 echo "--parameters required" && exit 1
#[[ -z "$entrypoint" ]] && entrypoint="/bin/deploy.sh"

kubectl apply -f "marketplace/crd/app-crd.yaml"

# Extract APP_INSTANCE_NAME and NAMESPACE from parameters.
name=$(echo "$parameters" | jq -r '.APP_INSTANCE_NAME')
namespace=$(echo "$parameters" | jq -r '.NAMESPACE')

# Create Namespace
kubectl create namespace "${namespace}"

# Create Application instance.
kubectl apply --namespace="$namespace" --filename=- <<EOF
apiVersion: app.k8s.io/v1alpha1
kind: Application
metadata:
  name: "${name}"
  namespace: "${namespace}"
spec:
  selector:
    matchLabels:
      app.kubernetes.io/name: "${name}"
  componentKinds:
  - kind: ConfigMap
  - kind: ServiceAccount
  - kind: RoleBinding
  - kind: Job
  assemblyPhase: "Pending"
EOF

# Fetch the server assigned uid for owner reference assignment.
application_uid=$(kubectl get "applications/$name" \
  --namespace="$namespace" \
  --output=jsonpath='{.metadata.uid}')

# Create RBAC role, service account, and role-binding.
# TODO(huyhg): Application should define the desired permissions,
# which should be transated into appropriate rules here instead of
# granting the role with all permissions.
kubectl apply --namespace="$namespace" --filename=- <<EOF
apiVersion: v1
kind: ServiceAccount
metadata:
  name: "${name}-deployer-sa"
  namespace: "${namespace}"
  labels:
    app.kubernetes.io/name: "${name}"
  ownerReferences:
  - apiVersion: "app.k8s.io/v1alpha1"
    kind: "Application"
    name: "${name}"
    uid: "${application_uid}"
    blockOwnerDeletion: true
---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: "${name}-deployer-rb"
  namespace: "${namespace}"
  labels:
    app.kubernetes.io/name: "${name}"
  ownerReferences:
  - apiVersion: "app.k8s.io/v1alpha1"
    kind: "Application"
    name: "${name}"
    uid: "${application_uid}"
    blockOwnerDeletion: true
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: "${name}-deployer-sa"
EOF

# Create ConfigMap (merging in passed in parameters).
kubectl apply --filename=- --output=json --dry-run <<EOF \
  | jq -s '.[0].data = .[1] | .[0]' \
      - <(echo "$parameters") \
  | kubectl apply --namespace="$namespace" --filename=-
apiVersion: v1
kind: ConfigMap
metadata:
  name: "${name}-deployer-config"
  labels:
    app.kubernetes.io/name: "${name}"
  namespace: "${namespace}"
  ownerReferences:
  - apiVersion: "app.k8s.io/v1alpha1"
    kind: "Application"
    name: "${name}"
    uid: "${application_uid}"
    blockOwnerDeletion: true
EOF

# If --pod=true passed, launch as a pod for debugging purposes
if [ -n "$debug_pod" ]; then
# Create deployer.
kubectl apply --namespace="$namespace" --filename=- <<EOF
apiVersion: v1
kind: Pod
metadata:
  name: "${name}-deployer-pod"
  labels:
    app.kubernetes.io/name: "${name}"
  ownerReferences:
  - apiVersion: "app.k8s.io/v1alpha1"
    kind: "Application"
    name: "${name}"
    uid: "${application_uid}"
    blockOwnerDeletion: true
spec:
  serviceAccountName: "${name}-deployer-sa"
  containers:
  - name: deployer
    image: "${deployer}"
    imagePullPolicy: IfNotPresent
    env:
    - name: DISABLE_CLOUDLAUNCHER_OVERRIDES
      value: "$disable_cl_overrides"
    volumeMounts:
    - name: config-volume
      mountPath: /data/values
    command: ["sh", "-c", "tail -f /dev/null"]
  restartPolicy: Never
  volumes:
  - name: config-volume
    configMap:
      name: "${name}-deployer-config"
EOF
else
# Create deployer.
kubectl apply --namespace="$namespace" --filename=- <<EOF
apiVersion: batch/v1
kind: Job
metadata:
  name: "${name}-deployer"
  labels:
    app.kubernetes.io/name: "${name}"
  ownerReferences:
  - apiVersion: "app.k8s.io/v1alpha1"
    kind: "Application"
    name: "${name}"
    uid: "${application_uid}"
    blockOwnerDeletion: true
spec:
  template:
    spec:
      serviceAccountName: "${name}-deployer-sa"
      containers:
      - name: deployer
        env:
        - name: DISABLE_CLOUDLAUNCHER_IMAGE_OVERRIDES
          value: "$disable_cl_overrides"
        image: "${deployer}"
        imagePullPolicy: Always
        volumeMounts:
        - name: config-volume
          mountPath: /data/values
        $([[ -z "$entrypoint" ]] || echo -n command: [\""${entrypoint}"\"])
      restartPolicy: Never
      volumes:
      - name: config-volume
        configMap:
          name: "${name}-deployer-config"
  backoffLimit: 0
EOF
fi
