#!/usr/bin/env bash

debug_pod=""

for i in "$@"
do
case $i in
  --deployer=*)
    deployer="${i#*=}"
    shift
    ;;
  --parameters=*)
    parameters="${i#*=}"
    shift
    ;;
  --entrypoint=*)
    entrypoint="${i#*=}"
    shift
    ;;
  --pod=true)
    debug_pod="TRUE"
    shift
    ;;
  *)
    >&2 echo "Unrecognized flag: $i"
    exit 1
    ;;
esac
done

# [[ -z "$deployer" ]] && >&2 echo "--deployer required" && exit 1
[[ -z "$parameters" ]] && >&2 echo "--parameters required" && exit 1
# [[ -z "$entrypoint" ]] && entrypoint="/bin/deploy.sh"

# Extract APP_INSTANCE_NAME and NAMESPACE from parameters.
name=$(echo "$parameters" | jq -r '.APP_INSTANCE_NAME')
namespace=$(echo "$parameters" | jq -r '.NAMESPACE')

if [ -n "$debug_pod" ]; then
  kubectl delete pod $name-deployer-pod -n "$namespace"
fi

# Gitlab chart specific cruft
kubectl delete ClusterRoleBinding "gitlab-shared-secrets"
kubectl delete serviceaccount "gitlab-shared-secrets" -n "$namespace"

kubectl delete namespace "$namespace"
