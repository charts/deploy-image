# Deprecated

This project is deprecated.

# GitLab Deploy Container

The GitLab deploy container is intended to provide an easy way to deploy the GitLab helm charts when running Tiller is not allowed. It also accepts configuration parameters to deploy in a automated way.

## Requirements

* All referenced container images utilized by the charts must be configurable to use alternate registries.
* The SIG-App CRD must be created and maintained for the deployed app.
* Tiller cannot be deployed into the cluster, it must run in client only mode.
* A JSON configuration file along with a temporary service account for installation can be provided as input
